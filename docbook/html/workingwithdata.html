<html><head><title>Chapter 3. Working With Data</title><link rel="stylesheet" type="text/css" href="../common/kde-default.css"><meta name="generator" content="DocBook XSL Stylesheets V1.79.1"><meta name="keywords" content="KDE, Kst, plotting, plot"><link rel="home" href="index.html" title="The Kst Handbook"><link rel="up" href="index.html" title="The Kst Handbook"><link rel="prev" href="tutorial-filters.html" title="Filters"><link rel="next" href="datamanager.html" title="The Data Manager"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="GENERATOR" content="KDE XSL Stylesheet V1.13 using libxslt"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr class="header"><td colspan="2"> </td></tr><tr id="logo"><td valign="top"><img src="../common/part_of_the_kde_family_horizontal_190.png" alt="Part of the KDE family" width="190" height="68" border="0"></td><td valign="middle" align="center" id="location"><h1>Working With Data</h1></td></tr></table><table width="100%" class="header"><tbody><tr><td align="left" class="navLeft" width="33%"><a accesskey="p" href="tutorial-filters.html">Prev</a></td><td align="center" class="navCenter" width="34%"> </td><td align="right" class="navRight" width="33%"> 
		      <a accesskey="n" href="datamanager.html">Next</a></td></tr></tbody></table><div class="chapter"><div class="titlepage"><div><div><h1 class="title"><a name="workingwithdata"></a>Chapter 3. Working With Data</h1></div></div></div><div class="toc"><p><b>Table of Contents</b></p><dl class="toc"><dt><span class="sect1"><a href="workingwithdata.html#datasources">Data Sources</a></span></dt><dd><dl><dt><span class="sect2"><a href="workingwithdata.html#DataSourceConcepts">Data Source Concepts</a></span></dt><dt><span class="sect2"><a href="workingwithdata.html#creatingascii">ASCII Input Files</a></span></dt></dl></dd><dt><span class="sect1"><a href="datamanager.html">The Data Manager</a></span></dt><dt><span class="sect1"><a href="datatypes.html">Data Types</a></span></dt><dd><dl><dt><span class="sect2"><a href="datatypes.html#vectors">Vectors</a></span></dt><dt><span class="sect2"><a href="datatypes.html#scalars">Scalars</a></span></dt><dt><span class="sect2"><a href="datatypes.html#curves">Curves</a></span></dt><dt><span class="sect2"><a href="datatypes.html#equations">Equations</a></span></dt><dt><span class="sect2"><a href="datatypes.html#Histograms">Histograms</a></span></dt><dt><span class="sect2"><a href="datatypes.html#power-spectra">Power Spectra</a></span></dt><dt><span class="sect2"><a href="datatypes.html#fits">Fits</a></span></dt><dt><span class="sect2"><a href="datatypes.html#filters">Filters</a></span></dt><dt><span class="sect2"><a href="datatypes.html#plugins">Standard Plugins</a></span></dt><dt><span class="sect2"><a href="datatypes.html#arrays">Matrices</a></span></dt><dt><span class="sect2"><a href="datatypes.html#images">Images</a></span></dt></dl></dd></dl></div><div class="sect1"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="datasources"></a>Data Sources</h2></div></div></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="DataSourceConcepts"></a>Data Source Concepts</h3></div></div></div><p>
A data source in <span class="application">Kst</span> is simply a supported data file.
Currently, Kst supports ASCII text files,
<a class="ulink" href="http://getdata.sourceforge.net/" target="_top">dirfiles</a>, and
<a class="ulink" href="http://www.unidata.ucar.edu/software/netcdf/" target="_top">NetCDF</a>,
for vectors and scalars, and FITS images, BIT image streams,
16 bit TIFF images, and any image format supported by 
<a class="ulink" href="http://doc.qt.io/qt-5/qimage.html" target="_top">QImage</a>,
 (jpg, png, bmp, etc) for images.
</p><p>

The following concepts are important in understanding how <span class="application">Kst</span> works with
different data sources.
Some terminology is also introduced in this section.
</p><div class="sect3"><div class="titlepage"><div><div><h4 class="title"><a name="DefineFields"></a>Fields</h4></div></div></div><p>
  Data in <span class="application">Kst</span> are accessed by field names.  A field name can refer to a single scalar or string, to a vector of values from a single sensor,
  or to a matrix.  For example, a column in an ASCII data file can be read in 
  as a vector.  An image in a png file can be read in as a matrix.  
  Datasource readers provide functions for reading and obtaining fields and 
  field names.
</p></div><div class="sect3"><div class="titlepage"><div><div><h4 class="title"><a name="Frames"></a>Frames</h4></div></div></div><p>
When reading in a vector from a data source field, data are addressed by their Frame number, not by their sample number.  Each field in a data source has its own fixed number of samples per frame.
</p><p>
For some data sources (eg, ASCII files) every frame contains exactly one sample (ie, for ASCII files, a frame is a valid row of data, and every row has exactly one sample for each field).  
</p><p>
However, for other data sources (eg, dirfiles), there may be multiple samples per frame.  In the illustration below, the first 3 frames of an imaginary dirfile are shown.  In this particular data file, Field1 has a 1 sample per frame, Field2 has 4 samples per frame, and Field3 has 2 samples per frame.  Every field must have a constant number of samples per frame throughout the file.
</p><p>
<span class="inlinemediaobject"><img src="Diagram-kst-frames.png"></span>
</p><p>
  In the figure, imagine that time proceeds from top to bottom.  <span class="application">Kst</span> assumes that the first sample in a frame is simultaneous for every field in the data source, and that the rest of the samples are sampled evenly throughout the frame, as shown.  
</p><p>  
  When plotting one vector against another, <span class="application">Kst</span> assumes that the first and last samples of each vector are simultaneous, and interpolates the shorter vector up to the resolution of the longer vector.  Since only the first sample in a frame can be assumed to be simultaneous across fields, <span class="emphasis"><em>when <span class="application">Kst</span> reads data into a vector, it only reads up to the first sample of the last frame requested,</em></span> so that plotting one vector against another will make sense.  The rest of the last frame will not be read.  
</p><p>
  So if the first three frames of Field1 and Field2 are read from the data source in the figure, 3 samples will be read from Field1, and 9 samples will be read from Field2 (ending at first sample of Frame 3) - not 12 as one might expect.
</p></div><div class="sect3"><div class="titlepage"><div><div><h4 class="title"><a name="supportingadditionalfileformatsdatasourceconceptsindex"></a>INDEX Field</h4></div></div></div><p>
As well as the explicit data fields in a data file, <span class="application">Kst</span> implicitly creates an INDEX field for all data sources.  The INDEX field is 1 sample per frame, and simply contains integers from 0 to N-1, where N is the number of frames in the data file.  It is common to plot vectors against INDEX.  This is convenient since  the INDEX of a sample or event is just the frame number, allowing easy identification and retrieval of events from a data file.
</p></div></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="creatingascii"></a>ASCII Input Files</h3></div></div></div><p>
<span class="application">Kst</span> is capable of reading vectors from a wide range of ASCII formats.  As long as the data are in columns, and as long as each non-comment row has the same number of columns, <span class="application">Kst</span> can probably read it.
</p><p>
  Consider reading this simple ASCII csv file:  each comma separated column represents a field. 
</p><div class="informalexample"><pre class="screen">
Length,Width
m,m
1.1,6.2
2.4,9.3
4.3,4.7
5.2,8.8
</pre></div><p>
  When you enter an ascii source into a data source selection widget (such as on the first page of the data wizard) the file will be identified as an ASCII file, and the <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="guiitem"><span class="guibutton">Configure</span></span> button will be enabled, as shown below.
</p><p>
<span class="inlinemediaobject"><img src="Screenshot-kst-datasource-selector.png"></span>
</p><p>
  Clicking on <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="guiitem"><span class="guibutton">Configure</span></span> will bring up the ASCII data source configuration dialog.
</p><p>
<span class="inlinemediaobject"><img src="Screenshot-kst-ascii-config.png"></span>
</p><p>
  Note that the first few lines of the file are shown.  The dialog in the screen shot has been filled out to read this file: looking at the first lines of the file, we see that data starts at line 3, line 1 holds the field names, and line 2 holds the units (which will be used by <span class="application">Kst</span> in plot labels).  Additionally, as this is a csv file, a "<code class="literal">,</code>" has been selected as the <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="guiitem"><span class="guilabel">Custom delimiter</span></span>.  Selecting <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="guiitem"><span class="guilabel">OK</span></span> will assign this configuration to this file.  <span class="application">Kst</span> will continue to use this configuration with this file until the configuration options are changed again in this dialog, or until <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="guiitem"><span class="guimenuitem">Clear datasource settings</span></span> in the <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="guiitem"><span class="guimenu">Settings</span></span> menu is selected.
</p><p>
</p></div></div></div><table width="100%" class="bottom-nav"><tr><td width="33%" align="left" valign="top" class="navLeft"><a href="tutorial-filters.html">Prev</a></td><td width="34%" align="center" valign="top" class="navCenter"><a href="index.html">Contents</a></td><td width="33%" align="right" valign="top" class="navRight"><a href="datamanager.html">Next</a></td></tr><tr><td width="33%" align="left" class="navLeft">Filters </td><td width="34%" align="center" class="navCenter"><a href="index.html">Up</a></td><td width="33%" align="right" class="navRight"> The Data Manager</td></tr></table></body></html>