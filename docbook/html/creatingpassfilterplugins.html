<html><head><title>Creating Pass Filter Plugins</title><link rel="stylesheet" type="text/css" href="../common/kde-default.css"><meta name="generator" content="DocBook XSL Stylesheets V1.79.1"><meta name="keywords" content="KDE, Kst, plotting, plot"><link rel="home" href="index.html" title="The Kst Handbook"><link rel="up" href="creatingplugins.html" title="Appendix A. Creating Additional Plugins"><link rel="prev" href="creatingnonlinearfitplugin.html" title="Creating Non-linear Fit Plugins"><link rel="next" href="supportingadditionalfileformats.html" title="Appendix B. Supporting Additional File Formats"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="GENERATOR" content="KDE XSL Stylesheet V1.13 using libxslt"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr class="header"><td colspan="2"> </td></tr><tr id="logo"><td valign="top"><img src="../common/part_of_the_kde_family_horizontal_190.png" alt="Part of the KDE family" width="190" height="68" border="0"></td><td valign="middle" align="center" id="location"><h1>Creating Pass Filter Plugins</h1></td></tr></table><table width="100%" class="header"><tbody><tr><td align="left" class="navLeft" width="33%"><a accesskey="p" href="creatingnonlinearfitplugin.html">Prev</a></td><td align="center" class="navCenter" width="34%">Creating Additional Plugins</td><td align="right" class="navRight" width="33%"> 
		      <a accesskey="n" href="supportingadditionalfileformats.html">Next</a></td></tr></tbody></table><div class="sect1"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="creatingpassfilterplugins"></a>Creating Pass Filter Plugins</h2></div></div></div><p>
<span class="application">Kst</span> provides header files to simplify the implementation of pass filter plugins. The use of these
header files is described below.
</p><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="creatingpassfilterpluginsheaderfiles"></a>Header Files</h3></div></div></div><p>
The pass filter header file is located in <code class="filename">kst/plugins/pass_filters</code> of
the <span class="application">Kst</span> source tarball.  The file is named <code class="filename">filters.h</code>
To use this file, include it in the source code for your plugin:
</p><pre class="screen">
#include &lt;../filters.h&gt;
</pre><p>
(by convention, we will place the source code for the plugin one directory below where the header files
are).
</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="creatingpassfilterpluginsrequirements"></a>Required Functions</h3></div></div></div><p>
The <code class="filename">filters.h</code> header file contains a single function that calculates the Fourier
transform of a supplied function, applies the supplied filter to the Fourier transform, and then calculates
the inverse Fourier transform of the filtered Fourier transform.  To supply the filter, the following
function needs to be implemented in the source code for your plugin:
</p><p><code class="function"><span class="returnvalue">double</span> filter_calculate( double <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>dFreqValue</code></em></span>, const double <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inScalars[]</code></em></span> )</code></p><p>
This function should calculate the filtered amplitude for the frequency <code class="literal">dFreqValue</code>.
<code class="literal">inScalars[]</code> will contain the unaltered input scalars for the plugin, specified in the
XML file.  Most likely <code class="literal">inScalars[]</code> will contain cutoff frequencies or other
properties of the filter.  For example, to implement a Butterworth high-pass filter,
<code class="function">filter_calculate</code> could be implemented as follows:
</p><div class="informalexample"><pre class="screen">
double filter_calculate( double dFreqValue, const double inScalars[] ) {
  double dValue;
  if( dFreqValue &gt; 0.0 ) {
    dValue = 1.0 / ( 1.0 +
    		pow( inScalars[1] / dFreqValue, 2.0 * (double)inScalars[0] ) );
  } else {
    dValue = 0.0;
  }
  return dValue;
}
</pre></div></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="creatingpassfilterpluginscallingfunction"></a>Calling the Filter Function</h3></div></div></div><p>
Once the required <code class="function">filter_calculate</code> has been implemented, the filter function
from the header file can be called:
</p><div class="literallayout"><p><code class="function">kst_pass_filter( <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inArrays</code></em></span>,<br>
                 <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inArrayLens</code></em></span>,<br>
                 <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inScalars</code></em></span>,<br>
                 <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outArrays</code></em></span>,<br>
                 <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outArrayLens</code></em></span>,<br>
                 <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outScalars</code></em></span> );</code></p></div><p>
The arguments supplied to the exported C function can usually be passed to
<code class="function">kst_pass_filter</code> without modification.  However, there are a few restrictions
on the arguments:
</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
<code class="varname">inArrays[0]</code> must contain the array of data to filter.
</p></li><li class="listitem"><p>
<code class="varname">inScalars</code> should contain the filter-specific parameters to be used by
the <code class="function">filter_calculate</code> function.
</p></li></ul></div><p>
After the function call, <code class="varname">outArrays[0]</code> will contain the filtered array of data, and
<code class="varname">outArrayLens</code> will be set appropriately.  The <code class="function">kst_pass_filter</code>
function does not use <code class="varname">outScalars</code>.
</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="creatingpassfilterpluginsexample"></a>Example</h3></div></div></div><p>
The following is an example of a pass filter plugin that implements the Butterworth high-pass filter.
</p><div class="informalexample"><pre class="screen">/*
 *  Butterworth low pass filter plugin for KST.
 *  Copyright 2004, The University of British Columbia
 *  Released under the terms of the GPL.
 */

#include &lt;stdlib.h&gt;
#include &lt;math.h&gt;
#include "../filters.h"

extern "C" int butterworth_highpass(const double *const inArrays[], const int inArrayLens[],
		const double inScalars[],
		double *outArrays[], int outArrayLens[],
		double outScalars[]);

int butterworth_highpass(const double *const inArrays[], const int inArrayLens[],
		const double inScalars[],
		double *outArrays[], int outArrayLens[],
		double outScalars[])
{
  int iReturn;

  iReturn = kst_pass_filter( inArrays,
                             inArrayLens,
                             inScalars,
                             outArrays,
                             outArrayLens,
                             outScalars );

  return iReturn;
}

double filter_calculate( double dFreqValue, const double inScalars[] ) {
  double dValue;

  if( dFreqValue &gt; 0.0 ) {
    dValue = 1.0 / ( 1.0 + pow( inScalars[1] / dFreqValue, 2.0 * (double)inScalars[0] ) );
  } else {
    dValue = 0.0;
  }

  return dValue;
}</pre></div></div></div><table width="100%" class="bottom-nav"><tr><td width="33%" align="left" valign="top" class="navLeft"><a href="creatingnonlinearfitplugin.html">Prev</a></td><td width="34%" align="center" valign="top" class="navCenter"><a href="index.html">Contents</a></td><td width="33%" align="right" valign="top" class="navRight"><a href="supportingadditionalfileformats.html">Next</a></td></tr><tr><td width="33%" align="left" class="navLeft">Creating Non-linear Fit Plugins </td><td width="34%" align="center" class="navCenter"><a href="creatingplugins.html">Up</a></td><td width="33%" align="right" class="navRight"> Supporting Additional File Formats</td></tr></table></body></html>