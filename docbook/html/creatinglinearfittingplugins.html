<html><head><title>Creating Linear Fit Plugins</title><link rel="stylesheet" type="text/css" href="../common/kde-default.css"><meta name="generator" content="DocBook XSL Stylesheets V1.79.1"><meta name="keywords" content="KDE, Kst, plotting, plot"><link rel="home" href="index.html" title="The Kst Handbook"><link rel="up" href="creatingplugins.html" title="Appendix A. Creating Additional Plugins"><link rel="prev" href="creatingplugins.html" title="Appendix A. Creating Additional Plugins"><link rel="next" href="creatingnonlinearfitplugin.html" title="Creating Non-linear Fit Plugins"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="GENERATOR" content="KDE XSL Stylesheet V1.13 using libxslt"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr class="header"><td colspan="2"> </td></tr><tr id="logo"><td valign="top"><img src="../common/part_of_the_kde_family_horizontal_190.png" alt="Part of the KDE family" width="190" height="68" border="0"></td><td valign="middle" align="center" id="location"><h1>Creating Linear Fit Plugins</h1></td></tr></table><table width="100%" class="header"><tbody><tr><td align="left" class="navLeft" width="33%"><a accesskey="p" href="creatingplugins.html">Prev</a></td><td align="center" class="navCenter" width="34%">Creating Additional Plugins</td><td align="right" class="navRight" width="33%"> 
		      <a accesskey="n" href="creatingnonlinearfitplugin.html">Next</a></td></tr></tbody></table><div class="sect1"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="creatinglinearfittingplugins"></a>Creating Linear Fit Plugins</h2></div></div></div><p>
To create a linear fit plugin, you could implement your own fitting algorithms and output the appropriate
vectors.  However, <span class="application">Kst</span> already comes with header files that make it easy for you to implement linear
least-squares fit plugins by just providing a few functions.
This section will describe how to take advantage of these files.
</p><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="headerslinearfittingplugins"></a>Header Files</h3></div></div></div><p>
Two header files are provided for performing linear fits, <code class="filename">linear.h</code>
 (for unweighted linear fits) and
<code class="filename">linear_weighted.h</code> (for weighted linear fits).  They are both located under
<code class="filename">kst/plugins/fits/</code> in the <span class="application">Kst</span> source tarball.  To use these files, include only one
of them in the source code for your plugin:
</p><pre class="screen">
#include &lt;../linear.h&gt;
</pre><p>
or
</p><pre class="screen">
#include &lt;../linear_weighted.h&gt;
</pre><p>
(by convention, we will place the source code for the plugin one directory below where the header files
are).
</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="reqfunctionsfittingplugins"></a>Implementing Required Functions</h3></div></div></div><p>
Given a general linear model:
</p><p>
<span class="inlinemediaobject"><img src="Formula-kst-generallinearmodel.png"></span>
</p><p>
where <code class="literal">y</code> is a vector of <code class="literal">n</code> observations, <code class="literal">X</code>
is an <code class="literal">n</code> by <code class="literal">p</code> matrix of predictor variables, and <code class="literal">c</code>
is the vector of <code class="literal">p</code> best-fit parameters that are to be estimated, the header files
provide functions for estimating <code class="literal">c</code> for a given <code class="literal">y</code> and
<code class="literal">X</code>.  To provide <code class="literal">X</code>, the following function needs to be
implemented in the source code for the plugin:
</p><div class="literallayout"><p><code class="function"><span class="returnvalue">double</span> calculate_matrix_entry( double <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>dX</code></em></span>, int <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>iPos</code></em></span> )</code></p></div><p>
</p><p>
This function should return the value of the entry in column <code class="literal">iPos</code>
of the matrix of predictor variables, using <code class="literal">x</code> value <code class="literal">dX</code>.
This function will be called by linear.h or linear_weighted.h.  The implementation of this function
depends on the model you wish to use for the fit, and is unique to each linear fit plugin.
For example, to fit to a polynomial model, <code class="function">calculate_matrix_entry</code> could
be implemented as follows:
</p><div class="informalexample"><pre class="screen">
double calculate_matrix_entry( double dX, int iPos ) {
  double dY;
  dY = pow( dX, (double)iPos );
  return dY;
}
</pre></div><p>
</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="callingfittingfunctionslinearfittingplugins"></a>Calling the Fitting Functions</h3></div></div></div><p>
Once the appropriate header file has been included and <code class="function">calculate_matrix_entry</code>
has been implemented, call the appropriate fitting function included from the header file:
</p><pre class="screen">
<code class="function">kstfit_linear_unweighted( <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inArrays</code></em></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inArrayLens</code></em></span>,
                          <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outArrays</code></em></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outArrayLens</code></em></span>,
                          <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outScalars</code></em></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>iNumParams</code></em></span> )</code>;
</pre><p>
or
</p><pre class="screen">
<code class="function">kstfit_linear_weighted( <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inArrays</code></em></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>inArrayLens</code></em></span>,
                        <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outArrays</code></em></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outArrayLens</code></em></span>,
                        <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>outScalars</code></em></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="parameter"><em class="parameter"><code>iNumParams</code></em></span> )</code>;
</pre><p>
</p><p>
Each function will return <code class="literal">0</code> on success, or <code class="literal">-1</code> on
error, so it is a good idea to set the return value of the exported C function to be equal to the return
value of the fitting function. To maintain simplicity, the code for the plugin can simply pass the
 arguments given to the exported C function to the fitting function. Note, however, that inArrays must
be structured as follows:
</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
<code class="varname">inArrays[0]</code> must contain the array of x coordinates of the data points
</p></li><li class="listitem"><p>
<code class="varname">inArrays[1]</code> must contain the array of y coordinates of the data points
</p></li><li class="listitem"><p>
<code class="varname">inArrays[2]</code> only exists if <code class="function">kstfit_linear_weighted</code>
is being called, and must contain the array of weights to use for the fit.
</p></li></ul></div><p>
The easiest way to ensure that inArrays is structured correctly is to specify the correct
order of input vectors in the XML file for the plugin.
</p><p>
<code class="varname">iNumParams</code> is the number of parameters in the fitting model used, which
should be equal to the number of columns in the matrix <code class="literal">X</code> of
predictor variables.  <code class="varname">iNumParams</code> must be set correctly before the fitting
function is called.
</p><p>
After <code class="function">kstfit_linear_unweighted</code> or <code class="function">kstfit_linear_weighted</code>
is called, <code class="varname">outArrays</code> and <code class="varname">outScalars</code>
will be set as follows:
</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
<code class="varname">outArrays[0]</code> will contain the array of fitted y values.
</p></li><li class="listitem"><p>
<code class="varname">outArrays[1]</code> will contain the array of residuals.
</p></li><li class="listitem"><p>
<code class="varname">outArrays[2]</code> will contain the array of best-fit parameters that were estimated.
</p></li><li class="listitem"><p>
<code class="varname">outArrays[3]</code> will contain the covariance matrix, returned row after row in an array.
</p></li><li class="listitem"><p>
<code class="varname">outScalars[0]</code> will contain chi^2/nu, where chi^2 is the weighted sum of squares of the residuals,
and nu is the degrees of freedom.
</p></li></ul></div><p>
<code class="varname">outArrayLens</code> will be correctly set to indicate the length of each output array.
</p><p>
Ensure that the specified outputs in the XML file match those that the exported C function returns (which
in most cases will simply be the outputs returned by the fitting function).
</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="examplelinearfittingplugins"></a>Example</h3></div></div></div><p>
The following is an example of the source code for a linear fit plugin.
</p><div class="informalexample"><pre class="screen">
/*
 *  Polynomial fitting plugin for KST.
 *  Copyright 2004, The University of British Columbia
 *  Released under the terms of the GPL.
 */

#include "../linear.h"

double calculate_matrix_entry( double dX, int iPos ) {
  double dY;

  dY = pow( dX, (double)iPos );

  return dY;
}

extern "C" int kstfit_polynomial_unweighted(
  const double *const inArrays[],
  const int inArrayLens[],
  const double inScalars[],
  double *outArrays[], int outArrayLens[],
  double outScalars[]);

int kstfit_polynomial_unweighted(
  const double *const inArrays[],
  const int inArrayLens[],
	const double inScalars[],
	double *outArrays[], int outArrayLens[],
	double outScalars[])
{
  int iRetVal = -1;
  int iNumParams;

  iNumParams = 1 + (int)floor( inScalars[0] );
  if( iNumParams &gt; 0 ) {
    iRetVal = kstfit_linear_unweighted( inArrays, inArrayLens,
                                        outArrays, outArrayLens,
                                        outScalars, iNumParams );
  }

  return iRetVal;
}
</pre></div></div></div><table width="100%" class="bottom-nav"><tr><td width="33%" align="left" valign="top" class="navLeft"><a href="creatingplugins.html">Prev</a></td><td width="34%" align="center" valign="top" class="navCenter"><a href="index.html">Contents</a></td><td width="33%" align="right" valign="top" class="navRight"><a href="creatingnonlinearfitplugin.html">Next</a></td></tr><tr><td width="33%" align="left" class="navLeft">Creating Additional Plugins </td><td width="34%" align="center" class="navCenter"><a href="creatingplugins.html">Up</a></td><td width="33%" align="right" class="navRight"> Creating Non-linear Fit Plugins</td></tr></table></body></html>